package tasksmanager

import (
	"math/rand"
)

// PriorityType describe type for priority field of task
type PriorityType int

const (
	PriorityVeryLow  PriorityType = 1
	PriorityLow      PriorityType = 2
	PriorityMedium   PriorityType = 3
	PriorityHigh     PriorityType = 4
	PriorityVeryHigh PriorityType = 5
)

const priorityLevelsCount = 5

// randomTaskPriority returns a random priority value given that a higher priority value falls out more often
func randomTaskPriority() PriorityType {
	i := rand.Intn(len(priorities))

	return priorities[i]
}

var priorities = []PriorityType{
	PriorityVeryLow,
	PriorityLow,
	PriorityLow,
	PriorityMedium,
	PriorityMedium,
	PriorityMedium,
	PriorityHigh,
	PriorityHigh,
	PriorityHigh,
	PriorityHigh,
	PriorityVeryHigh,
	PriorityVeryHigh,
	PriorityVeryHigh,
	PriorityVeryHigh,
	PriorityVeryHigh,
}
