package tasksmanager

// TaskEventHandler describes handler for task evensts such as create, run and complete
type TaskEventHandler func(task *Task)

// TaskEventErrorHandler describes task process error handler
type TaskEventErrorHandler func(task *Task, err error)

// Task describes task
type Task struct {
	// Type contain arbitrary value by which the client can determine the type of task
	Type int

	priority PriorityType
	onCreate,
	onRun,
	onComplete []TaskEventHandler
	onError []TaskEventErrorHandler
}

// NewTask create new task
func NewTask(taskType int, priority PriorityType, onCreate, onRun, onComplete []TaskEventHandler, onError []TaskEventErrorHandler) *Task {
	return &Task{
		Type:       taskType,
		priority:   priority,
		onCreate:   onCreate,
		onRun:      onRun,
		onError:    onError,
		onComplete: onComplete,
	}
}
