module gitlab.com/alex.dogonin/tasks-manager

go 1.12

require (
	github.com/stretchr/testify v1.4.0
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0 // indirect
	golang.org/x/sync v0.0.0-20190423024810-112230192c58 // indirect
)
