// Package tasksmanager provide tool for organize tasks queue with priority supporting
package tasksmanager

// TasksHandler describe tasks processor for tasks manager
type TasksHandler func(*Task) error

// TasksManager puts tasks to queue and send them to processing
type TasksManager struct {
	handler      TasksHandler
	workersCount int

	tasksChanels []chan *Task
	stopCh       chan struct{}
}

// NewTasksManager creates new tasks manager
func NewTasksManager(handler TasksHandler, workersCount int) *TasksManager {
	manager := &TasksManager{
		handler:      handler,
		workersCount: workersCount,

		tasksChanels: make([]chan *Task, priorityLevelsCount),
		stopCh:       make(chan struct{}, 1),
	}

	for i := range manager.tasksChanels {
		manager.tasksChanels[i] = make(chan *Task, workersCount)
	}

	go manager.run()

	return manager
}

// Put puts task to queue
func (tm *TasksManager) Put(task *Task) {
	tm.tasksChanels[tm.priorityToIndex(task.priority)] <- task

	for _, evHandler := range task.onCreate {
		evHandler(task)
	}
}

// Stop safety stops tasks processing
func (tm *TasksManager) Stop() {
	tm.stopCh <- struct{}{}
}

// run infinity loop for tasks processing
func (tm *TasksManager) run() {
	defer close(tm.stopCh)
	defer func() {
		for _, ch := range tm.tasksChanels {
			close(ch)
		}
	}()

	semaphoreCh := make(chan struct{}, tm.workersCount)
	for i := 0; i < tm.workersCount; i++ {
		semaphoreCh <- struct{}{}
	}
	defer close(semaphoreCh)

	for {
		select {
		case <-tm.stopCh:
			return
		default:
		}

		<-semaphoreCh

		priority := randomTaskPriority()
		var task *Task

		select {
		case <-tm.stopCh:
			return
		case task = <-tm.tasksChanels[tm.priorityToIndex(priority)]:
		case task = <-tm.tasksChanels[tm.priorityToIndex(PriorityVeryHigh)]:
		case task = <-tm.tasksChanels[tm.priorityToIndex(PriorityHigh)]:
		case task = <-tm.tasksChanels[tm.priorityToIndex(PriorityMedium)]:
		case task = <-tm.tasksChanels[tm.priorityToIndex(PriorityLow)]:
		case task = <-tm.tasksChanels[tm.priorityToIndex(PriorityVeryLow)]:
		}

		go func() {
			defer func() { semaphoreCh <- struct{}{} }()

			for _, evHandler := range task.onRun {
				evHandler(task)
			}

			if err := tm.handler(task); err != nil {
				for _, evHandler := range task.onError {
					evHandler(task, err)
				}
				return
			}

			for _, evHandler := range task.onComplete {
				evHandler(task)
			}
		}()
	}

}

func (*TasksManager) priorityToIndex(priority PriorityType) int {
	switch priority {
	case PriorityVeryLow:
		return 0
	case PriorityLow:
		return 1
	case PriorityMedium:
		return 2
	case PriorityHigh:
		return 3
	case PriorityVeryHigh:
		return 4
	}

	panic("incorrect priority value")
}
