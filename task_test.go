package tasksmanager

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTask(t *testing.T) {
	onCreate := []TaskEventHandler{func(*Task) {}}
	onRun := []TaskEventHandler{func(*Task) {}}
	onComplete := []TaskEventHandler{func(*Task) {}}
	onError := []TaskEventErrorHandler{func(*Task, error) {}}

	expected := &Task{
		Type:       12345678,
		priority:   PriorityMedium,
		onCreate:   onCreate,
		onRun:      onRun,
		onComplete: onComplete,
		onError:    onError,
	}

	actual := NewTask(
		12345678,
		PriorityMedium,
		onCreate,
		onRun,
		onComplete,
		onError,
	)

	assert.Equal(t, expected, actual)
}
