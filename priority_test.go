package tasksmanager

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_randomTaskPriority(t *testing.T) {
	priorityCount := make(map[PriorityType]int)

	const (
		veryLowExpectedCount  = 1
		lowExpectedCount      = 2
		middleExpectedCount   = 3
		highExpectedCount     = 4
		veryHighExpectedCount = 5

		delta = 50

		loopLen = 4500
	)

	for i := 0; i < loopLen; i++ {
		p := randomTaskPriority()
		priorityCount[p]++
	}

	veryLowCount := float32(priorityCount[PriorityVeryLow]) / 300
	assert.InDelta(t, veryLowExpectedCount, veryLowCount, delta, "unexpected very low count")

	lowCount := float32(priorityCount[PriorityLow]) / 300
	assert.InDelta(t, lowExpectedCount, lowCount, delta, "unexpected low count")

	middleCount := float32(priorityCount[PriorityMedium]) / 300
	assert.InDelta(t, middleExpectedCount, middleCount, delta, "unexpected middle count")

	highCount := float32(priorityCount[PriorityHigh]) / 300
	assert.InDelta(t, highExpectedCount, highCount, delta, "unexpected high count")

	veryHighCount := float32(priorityCount[PriorityVeryHigh]) / 300
	assert.InDelta(t, veryHighExpectedCount, veryHighCount, delta, "unexpected very high count")
}
