package tasksmanager

import (
	"testing"

	"time"

	"errors"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestNewTasksManager(t *testing.T) {
	handlerMock := handlerMock{}

	actual := NewTasksManager(handlerMock.handleTask, 1)

	assert.NotNil(t, actual.handler, "undefined handler")
	assert.Equal(t, 1, actual.workersCount, "unexpected workers count")
	assert.NotNil(t, actual.tasksChanels, "undefined tasks chanel")
	assert.Equal(t, 5, len(actual.tasksChanels), "unexpected tasks chanel length")
	assert.NotNil(t, actual.tasksChanels[0], "undefined very low priority tasks chanel")
	assert.NotNil(t, actual.tasksChanels[1], "undefined low priority tasks chanel")
	assert.NotNil(t, actual.tasksChanels[2], "undefined middle priority tasks chanel")
	assert.NotNil(t, actual.tasksChanels[3], "undefined high priority tasks chanel")
	assert.NotNil(t, actual.tasksChanels[4], "undefined very high priority tasks chanel")
	assert.NotNil(t, actual.stopCh, "undefined stop chanel")
}

func TestTasksManager_Put(t *testing.T) {
	manager := TasksManager{
		tasksChanels: make([]chan *Task, priorityLevelsCount),
	}
	for i := range manager.tasksChanels {
		manager.tasksChanels[i] = make(chan *Task, 1)
	}

	expected := NewTask(123, PriorityMedium, []TaskEventHandler{}, []TaskEventHandler{}, []TaskEventHandler{}, []TaskEventErrorHandler{})

	manager.Put(expected)

	actual, ok := <-manager.tasksChanels[manager.priorityToIndex(expected.priority)]

	assert.True(t, ok)
	assert.Equal(t, expected, actual)
}

func TestTasksManager_Stop(t *testing.T) {
	manager := TasksManager{
		stopCh: make(chan struct{}, 1),
	}

	manager.Stop()

	_, ok := <-manager.stopCh

	assert.True(t, ok)
}

func TestTasksManager_run(t *testing.T) {
	task := NewTask(123, PriorityMedium, []TaskEventHandler{}, []TaskEventHandler{}, []TaskEventHandler{}, []TaskEventErrorHandler{})

	handlers := handlerMock{}
	handlers.On("handleTask", task).Return(nil)

	manager := NewTasksManager(handlers.handleTask, 1)
	manager.Put(task)

	time.Sleep(500 * time.Millisecond)

	handlers.AssertExpectations(t)
}

func TestTasksManager_run_workersCount(t *testing.T) {
	task := NewTask(123, PriorityMedium, []TaskEventHandler{}, []TaskEventHandler{}, []TaskEventHandler{}, []TaskEventErrorHandler{})

	handlers := handlerMock{}
	handlers.On("handleTask", mock.Anything).Return(nil).WaitUntil(time.Tick(2 * time.Second))

	manager := NewTasksManager(handlers.handleTask, 1)
	manager.Put(task)
	manager.Put(task)

	time.Sleep(500 * time.Millisecond)

	actual, ok := <-manager.tasksChanels[manager.priorityToIndex(task.priority)]

	assert.True(t, ok)
	assert.Equal(t, task, actual)
}

func TestTasksManager_run_Stop(t *testing.T) {
	task := NewTask(123, PriorityMedium, []TaskEventHandler{}, []TaskEventHandler{}, []TaskEventHandler{}, []TaskEventErrorHandler{})

	handlers := handlerMock{}

	manager := &TasksManager{
		handler:      handlers.handleTask,
		workersCount: 1,

		tasksChanels: make([]chan *Task, priorityLevelsCount),
		stopCh:       make(chan struct{}, 1),
	}
	for i := range manager.tasksChanels {
		manager.tasksChanels[i] = make(chan *Task, 1)
	}

	manager.Stop()
	go manager.run()
	time.Sleep(500 * time.Millisecond)

	assert.Panics(t, func() { manager.Put(task) })

	_, ok := <-manager.stopCh
	assert.False(t, ok)

	_, ok = <-manager.tasksChanels[0]
	assert.False(t, ok)

	_, ok = <-manager.tasksChanels[1]
	assert.False(t, ok)

	_, ok = <-manager.tasksChanels[2]
	assert.False(t, ok)

	_, ok = <-manager.tasksChanels[3]
	assert.False(t, ok)

	_, ok = <-manager.tasksChanels[3]
	assert.False(t, ok)

	handlers.AssertExpectations(t)
}

func TestTasksManager_run_tasksProcessing(t *testing.T) {
	handlers := handlerMock{}

	veryLowPriorityTask := NewTask(
		1,
		PriorityVeryLow,
		[]TaskEventHandler{handlers.eventHandlerCreate},
		[]TaskEventHandler{handlers.eventHandlerRun},
		[]TaskEventHandler{handlers.eventHandlerComplete},
		[]TaskEventErrorHandler{handlers.eventHandlerError},
	)
	lowPriorityTask := NewTask(
		2,
		PriorityLow,
		[]TaskEventHandler{handlers.eventHandlerCreate},
		[]TaskEventHandler{handlers.eventHandlerRun},
		[]TaskEventHandler{handlers.eventHandlerComplete},
		[]TaskEventErrorHandler{handlers.eventHandlerError},
	)
	mediumPriorityTask := NewTask(
		3,
		PriorityMedium,
		[]TaskEventHandler{handlers.eventHandlerCreate},
		[]TaskEventHandler{handlers.eventHandlerRun},
		[]TaskEventHandler{handlers.eventHandlerComplete},
		[]TaskEventErrorHandler{handlers.eventHandlerError},
	)
	highPriorityTask := NewTask(
		4,
		PriorityHigh,
		[]TaskEventHandler{handlers.eventHandlerCreate},
		[]TaskEventHandler{handlers.eventHandlerRun},
		[]TaskEventHandler{handlers.eventHandlerComplete},
		[]TaskEventErrorHandler{handlers.eventHandlerError},
	)
	veryHighPriorityTask := NewTask(
		5,
		PriorityVeryHigh,
		[]TaskEventHandler{handlers.eventHandlerCreate},
		[]TaskEventHandler{handlers.eventHandlerRun},
		[]TaskEventHandler{handlers.eventHandlerComplete},
		[]TaskEventErrorHandler{handlers.eventHandlerError},
	)
	errorTask := NewTask(
		6,
		PriorityMedium,
		[]TaskEventHandler{handlers.eventHandlerCreate},
		[]TaskEventHandler{handlers.eventHandlerRun},
		[]TaskEventHandler{handlers.eventHandlerComplete},
		[]TaskEventErrorHandler{handlers.eventHandlerError},
	)

	handlers.On("handleTask", veryLowPriorityTask).Return(nil)
	handlers.On("eventHandlerCreate", veryLowPriorityTask)
	handlers.On("eventHandlerRun", veryLowPriorityTask)
	handlers.On("eventHandlerComplete", veryLowPriorityTask)

	handlers.On("handleTask", lowPriorityTask).Return(nil)
	handlers.On("eventHandlerCreate", lowPriorityTask)
	handlers.On("eventHandlerRun", lowPriorityTask)
	handlers.On("eventHandlerComplete", lowPriorityTask)

	handlers.On("handleTask", mediumPriorityTask).Return(nil)
	handlers.On("eventHandlerCreate", mediumPriorityTask)
	handlers.On("eventHandlerRun", mediumPriorityTask)
	handlers.On("eventHandlerComplete", mediumPriorityTask)

	handlers.On("handleTask", highPriorityTask).Return(nil)
	handlers.On("eventHandlerCreate", highPriorityTask)
	handlers.On("eventHandlerRun", highPriorityTask)
	handlers.On("eventHandlerComplete", highPriorityTask)

	handlers.On("handleTask", veryHighPriorityTask).Return(nil)
	handlers.On("eventHandlerCreate", veryHighPriorityTask)
	handlers.On("eventHandlerRun", veryHighPriorityTask)
	handlers.On("eventHandlerComplete", veryHighPriorityTask)

	handlers.On("handleTask", errorTask).Return(errors.New("test error"))
	handlers.On("eventHandlerCreate", errorTask)
	handlers.On("eventHandlerRun", errorTask)
	handlers.On("eventHandlerError", errorTask, mock.Anything)

	manager := NewTasksManager(handlers.handleTask, 1)
	manager.Put(veryLowPriorityTask)
	manager.Put(lowPriorityTask)
	manager.Put(mediumPriorityTask)
	manager.Put(highPriorityTask)
	manager.Put(veryHighPriorityTask)
	manager.Put(errorTask)

	time.Sleep(500 * time.Millisecond)

	handlers.AssertNumberOfCalls(t, "eventHandlerComplete", 5)
	handlers.AssertNumberOfCalls(t, "eventHandlerRun", 6)
	handlers.AssertNumberOfCalls(t, "eventHandlerCreate", 6)
	handlers.AssertNumberOfCalls(t, "eventHandlerError", 1)
	handlers.AssertExpectations(t)
}

func TestTaskManager_priorityToIndex(t *testing.T) {
	manager := TasksManager{}

	assert.Equal(t, 0, manager.priorityToIndex(PriorityVeryLow))
	assert.Equal(t, 1, manager.priorityToIndex(PriorityLow))
	assert.Equal(t, 2, manager.priorityToIndex(PriorityMedium))
	assert.Equal(t, 3, manager.priorityToIndex(PriorityHigh))
	assert.Equal(t, 4, manager.priorityToIndex(PriorityVeryHigh))
	assert.Panics(t, func() {
		manager.priorityToIndex(PriorityType(99))
	})
}

type handlerMock struct {
	mock.Mock
}

func (mock *handlerMock) handleTask(task *Task) error {
	args := mock.Called(task)

	return args.Error(0)
}

func (mock *handlerMock) eventHandlerComplete(task *Task) {
	mock.Called(task)
}

func (mock *handlerMock) eventHandlerRun(task *Task) {
	mock.Called(task)
}

func (mock *handlerMock) eventHandlerCreate(task *Task) {
	mock.Called(task)
}

func (mock *handlerMock) eventHandlerError(task *Task, err error) {
	mock.Called(task, err)
}
